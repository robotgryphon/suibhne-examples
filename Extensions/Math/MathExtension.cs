﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ostenvighx.Suibhne.Extensions;
using NetworkBase = Ostenvighx.Suibhne.Networks.Base;
using NCalc;

namespace Math_Extension {
    class MathExtension : Extension {

        [CommandHandler("parseMath"), CommandHelp("Give a basic mathematical expression to parse. (1 + 2 * 3, etc)")]
        public void ParseMath(Extension ext, NetworkBase.Message msg) {
            NetworkBase.Message response = NetworkBase.Message.GenerateResponse(new NetworkBase.User(ext.Name.Replace(" ", "_")), msg); 
            
            try {
                Expression e = new Expression(msg.message);

                
                response.message = "Result: " + e.Evaluate().ToString();
                ext.SendMessage(response);
            }

            catch (Exception e) {
                response.message = "Error: " + e.Message;
                ext.SendMessage(response);
            }
        }
    }
}
