﻿using Ostenvighx.Suibhne.Extensions;
using Ostenvighx.Suibhne.Networks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SQLite;
using System.Data;

using System.IO;

using Newtonsoft.Json.Linq;
using Ostenvighx.Suibhne.Networks.Base;

namespace Greeter {
    
    public class GreeterExtension : Extension {

        public SQLiteConnection db;
        protected List<String> Greetings;

        public GreeterExtension()
            : base() {
                this.db = new SQLiteConnection("Data Source=" + Environment.CurrentDirectory + @"\db.sns");
                this.Greetings = new List<String>();

                String greetingsTemplateFile = Environment.CurrentDirectory + @"\greetings.json";
                if (File.Exists(greetingsTemplateFile)) {
                    try {
                        JObject greetsFile = JObject.Parse(File.ReadAllText(greetingsTemplateFile));
                        JArray greets = (JArray) greetsFile["templates"];
                        foreach (String template in greets) {
                            Greetings.Add(template);
                            Console.WriteLine("Registered template " + template);
                        }

                        Console.WriteLine();
                    }

                    catch (Exception e) {
                        Console.WriteLine("Failure to parse template file.");
                        Greetings.Add("waves to {Name}, calling out a description: '{Message}'");
                    }
                } else {
                    Greetings.Add("waves to {Name}, calling out a description: '{Message}'");
                }      
        }

        public string GetUserDescription(User u) {
            string returned = null;
            try {
                if(db.State != ConnectionState.Open) db.Open();
                SQLiteCommand c = db.CreateCommand();
                c.CommandText = "SELECT * FROM Descriptions WHERE lower(DisplayName) = '" + u.DisplayName.ToLower() + "';";
                SQLiteDataReader sdr = c.ExecuteReader();
                DataTable greeting = new DataTable();
                greeting.Load(sdr);

                if (greeting.Rows.Count > 0) {
                    returned = greeting.Rows[0]["Message"].ToString();
                }
            }

            catch (Exception e) {
                Console.WriteLine("Error getting description: " + e);
            }

            finally {
                db.Close();
            }

            return returned;
        }

        public Boolean UpdateOrAddDescription(String user, String desc) {
            try {
                if (db.State != ConnectionState.Open) db.Open();
                SQLiteCommand c = db.CreateCommand();
                c.CommandText = "SELECT count(*) as added FROM Descriptions WHERE lower(DisplayName) = '" + user.ToLower() + "';";
                SQLiteDataReader sdr = c.ExecuteReader();
                DataTable greeting = new DataTable();
                greeting.Load(sdr);

                if (greeting.Rows[0]["added"].ToString() == "1") {
                    // Update description
                    c.CommandText = "UPDATE Descriptions SET Message = '" + desc + "' WHERE lower(DisplayName) = '" + user.ToLower() + "');";
                    int updated = c.ExecuteNonQuery();
                    if (updated == 1)
                        return true;
                    else
                        return false;
                } else {
                    // Add description
                    c.CommandText = "INSERT INTO Descriptions VALUES ('" + user.ToLower() + "', '" + desc + "', '" + DateTime.Now.ToString() + "');";
                    int updated = c.ExecuteNonQuery();
                    if (updated == 1)
                        return true;
                    else
                        return false;
                }
            }

            catch (Exception) {
                return false;
            }

            finally {
                db.Close();
            }

            return true;
        }

        [CommandHandler("greeterConfig")]
        public void HandleConfigurationCommand(Extension ext, JObject e) {

            Message response = new Message(Guid.Parse(e["location"]["id"].ToString()), new User(ext.Name), "");

            String[] args = e["arguments"].ToString().Split(' ');
            if (args[0] != "") {
                switch (args[0].ToLower()) {
                    case "lurk":
                    case"lurkmode":

                        if (e["location"]["type"] != null &&
                            !e["location"]["type"].ToString().StartsWith("private")) {
                            response.message = "You cannot change lurk mode for private locations.";
                            ext.SendMessage(response);

                            return;
                        }

                        if (args.Length > 1) {
                            switch (args[1].ToLower()) {
                                case "enable":
                                case "on":
                                case "true":
                                    response.message = "Toggling lurk mode ON for location " + e["location"]["id"].ToString() + ".";
                                    ext.SendMessage(response);
                                    break;

                                case "disable":
                                case "off":
                                case "false":
                                    response.message = "Toggling lurk mode OFF for location " + e["location"]["id"].ToString() + ".";
                                    ext.SendMessage(response);
                                    break;

                                default:
                                    response.message = "Invalid toggle. Specify lurk on/enable/true or off/disable/false.";
                                    ext.SendMessage(response);
                                    break;
                            }
                        } else {
                            response.message = "Need option to enable or disable lurk mode.";
                            ext.SendMessage(response);
                        }
                        break;

                    default:
                        response.message = "Invalid arguments. Available args: {lurk}";
                        ext.SendMessage(response);
                        break;
                }
            }

        }

        [CommandHandler("edit"), CommandHelp("Edits the current location's greeting. You can also set whether the greeting in this location should be global or local.")]
        public void HandleEditCommand(Extension ext, JObject e) {

            string args = e["arguments"].ToString();
            if (args.Trim() == "") {

            } else {
                try {
                    Message response = new Message(e["location"]["id"].ToObject<Guid>(), new User(), "");
                    User sender = new User(e["sender"]["DisplayName"].ToString());
                    sender.UniqueName = e["sender"]["Username"].ToString();

                    string[] argsParts = args.Trim().Split(' ');

                    // Send error message, need an option.
                    byte locationType = e["location"]["type"].ToObject<byte>();

                    // Set up private message if needed
                    if (Message.IsPrivateMessage((Reference.MessageType)locationType)) {
                        response.type = Reference.MessageType.PrivateMessage;
                        response.target = sender;
                    }

                    switch (argsParts[0].ToLower()) {
                        case "modify":
                        case "edit":
                            string newDesc = args.Substring(args.IndexOf(' ') + 1);
                            bool done = ((GreeterExtension) ext).UpdateOrAddDescription(sender.DisplayName, newDesc);
                            if (done)
                                response.message = "Description successfully modified.";
                            else
                                response.message = "Error updating description. Try again, checking if you don't have odd characters in the message.";

                            ext.SendMessage(response);
                            break;

                        case "check":
                            string desc = ((GreeterExtension)ext).GetUserDescription(sender);
                            if (desc == null)
                                response.message = "Your description is not set up. Please set it up with the edit or modify option.";
                            else
                                response.message = "Your description: " + desc;

                            ext.SendMessage(response);
                            break;

                        case "type":
                            response.message = "Sorry, but local vs global descriptions are still being worked on.";
                            ext.SendMessage(response);
                            break;

                        default:

                            response.message = "Please specify something to do. Arguments are edit/modify, check, or type.";
                            ext.SendMessage(response);

                            break;
                    }
                }

                catch (Exception exc) {
                    Console.WriteLine(exc);
                }
            }
        }

        [CommandHandler("query"), CommandHelp("Give the query command a nickname to query on. If desired, include a location name afterwards. (!command <name> [<location>])")]
        public void HandleQueryCommand(Extension ext, JObject e) {
            string args = e["arguments"].ToString();
            if (args.Trim() == "") {

            } else {
                try {
                    Message response = new Message(e["location"]["id"].ToObject<Guid>(), new User(), "");
                    User sender = new User(e["sender"]["DisplayName"].ToString());
                    sender.UniqueName = e["sender"]["Username"].ToString();

                    string[] argsParts = args.Trim().Split(' ');

                    // Send error message, need an option.
                    byte locationType = e["location"]["type"].ToObject<byte>();

                    // Set up private message if needed
                    if (Message.IsPrivateMessage((Reference.MessageType)locationType)) {
                        response.type = Reference.MessageType.PrivateMessage;
                        response.target = sender;
                    }

                    String desc = ((GreeterExtension) ext).GetUserDescription(new User(argsParts[0]));
                    if (desc == null)
                        response.message = "That user's description is not set up.";
                    else
                        response.message = argsParts[0] + ". \"" + desc + "\"";

                    ext.SendMessage(response);

                }

                catch (Exception) { }
            }
        }

        protected override void HandleUserEvent(String json) {

            JObject e = JObject.Parse(json);
            User u = new User();
            u.DisplayName = e["user"]["DisplayName"].ToString();
            u.UniqueName = e["user"]["Username"].ToString();

            try {
                
                switch (e["event"].ToString().ToLower()) {
                    case "user.join":

                        string desc = GetUserDescription(u);
                        if (desc == null) {

                        } else {
                            int randomGreetIndex = new Random().Next(Greetings.Count);

                            String greetTemplate = Greetings.ElementAt(randomGreetIndex);

                            Message response = new Message(Guid.Parse(e["location"].ToString()), new User(this.Name), greetTemplate);
                            response.type = Reference.MessageType.PublicAction;

                            response.message = response.message.Replace("{Name}", u.DisplayName);
                            response.message = response.message.Replace("{Message}", desc);

                            this.SendMessage(response);

                            SQLiteCommand c = db.CreateCommand();
                            c.CommandText = "UPDATE Descriptions SET LastSeen='" + DateTime.Now + "' WHERE lower(DisplayName)='" + u.DisplayName.ToLower() + "';";
                            c.ExecuteNonQuery();
                        }

                    
                        break;

                    default:
                        // Do nothing, this is just a greeter
                        break;
                }
            }

            catch (Exception ex) {
                Console.WriteLine(ex);
            }

            finally {
                db.Close();
            }            
        }
    }
}
