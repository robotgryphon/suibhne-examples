﻿
using Newtonsoft.Json.Linq;
using Ostenvighx.Suibhne.Extensions;
using Ostenvighx.Suibhne.Networks.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Ostenvighx.Suibhne.Dice {

    class DieRollResult {
        internal string format;
        internal int[] values;
        public long total;

        public override string ToString() {
            return this.total.ToString();
        }
    }

    class DiceExtension : Ostenvighx.Suibhne.Extensions.Extension {

        private static Regex DieFormat = new Regex(@"(?<dice>\d+)d(?<sides>\d+)(?<mod>[\+\-]\d+)?", RegexOptions.None);

        protected override void Activate() {
            JObject activation = new JObject();
            activation.Add("event", "extension_activation");
            activation.Add("extid", Identifier);
            activation.Add("name", Name);

            activation.Add("required_events", new JArray(new string[] { "command_recieved" }));
            activation.Add("optional_events", new JArray());

            conn.Send(Encoding.UTF32.GetBytes(activation.ToString()));
        }

        /// <summary>
        /// Calculates the value of a die roll in standard format.
        /// </summary>
        /// <param name="die">Standard fie rolling format, aka 1d6+4.</param>
        /// <returns></returns>
        protected DieRollResult GetDiceValue(String die) {

            DieRollResult result = new DieRollResult();

            Match dice = DieFormat.Match(die);
            if (dice.Success) {

                result.format = die;

                String numDiceString = dice.Groups["dice"].Value;
                String numSidesString = dice.Groups["sides"].Value;
                String modString = dice.Groups["mod"].Value;

                try {
                    int numDice = int.Parse(numDiceString);
                    int numSides = int.Parse(numSidesString);
                    int mod = 0;

                    result.values = new int[numDice];

                    if (modString != "")
                        mod = int.Parse(modString.TrimStart(new char[] { '+', '-' }));

                    if (numDice >= 1 && numDice <= 500) {
                        if (numSides <= 10000 && numSides >= 1) {
                            if (mod < 5000000) {
                                Random randomizer = new Random();
                                for (int i = 0; i < numDice; i++) {
                                    result.values[i] = randomizer.Next(1, numSides+1);
                                    

                                    result.total += result.values[i];
                                }

                                if (modString.StartsWith("-")) mod = -mod;
                                result.total += mod;
                            }
                        }
                    }
                }

                catch (FormatException) {
                    throw;
                }

                catch (Exception) { }
            }

            return result;
        }

        [CommandHandler("rollDice"), CommandHelp("Roll up to ten dice, separated by spaces. Format is standard, XdY+Z. Modifier (+Z) can be negative or excluded.")]
        public void DoDiceRoll(Extension ext, String json){

            JObject e;
            try { e = JObject.Parse(json); }
            catch (Exception) { return; }

            List<DieRollResult> rolls = new List<DieRollResult>();
            Message response = new Message(e["location"]["id"].ToObject<Guid>(), new User(ext.Name), "");

            long total = 0;
            String args = e["arguments"].ToString().Trim();

            // If we have no arguments
            if (args == "") {
                response.message = "Roll up to ten dice, separated by spaces. Format is standard, XdY+Z. Modifier (+Z) can be negative or excluded.";
                ext.SendMessage(response);
                return;
            }

            String[] args2 = args.Split(' ');
            if (args2.Length >= 1 && args2.Length <= 10) {

                for (int die = 1; die < args2.Length + 1; die++) {
                    DieRollResult result = GetDiceValue(args2[die - 1].ToString());
                    total += (result.total);
                    rolls.Add(result);
                }


                if (Message.IsPrivateMessage((Reference.MessageType)e["location"]["type"].ToObject<byte>())) {
                    response.type = Networks.Base.Reference.MessageType.PrivateAction;
                    response.target = new User(e["sender"]["DisplayName"].ToString());
                }  else
                    response.type = Networks.Base.Reference.MessageType.PublicAction;

                response.message = "rolls a few dice, and the results are: " + total + "!";

                ext.SendMessage(response);
            } else {
                response.message =  "Up to ten dice can be rolled. (You had " + (args2.Length - 1) + 
                    "). Format is 1d20(+1), up to ten dice (put a space between the dice notations).";

                ext.SendMessage(response);
            }
        }

    }
}
