﻿using Newtonsoft.Json.Linq;
using Ostenvighx.Suibhne.Extensions;
using Ostenvighx.Suibhne.Networks.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Youtube_Information {
    internal class Youtube : Extension {

        private static Regex URL_MATCH = new Regex(@"^.*(?:(?:youtu\.be\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))(?<id>[^#\&\?]*).*");

        protected override void Activate() {
            JObject activation = new JObject();
            activation.Add("event", "extension_activation");
            activation.Add("extid", Identifier);
            activation.Add("name", Name);

            activation.Add("required_events", new JArray(new string[] { "message_recieved" }));
            activation.Add("optional_events", new JArray());

            conn.Send(Encoding.UTF32.GetBytes(activation.ToString()));
        }

        protected override void HandleIncomingMessage(string json) {
            JObject message;
            try { message = JObject.Parse(json); }
            catch (Exception) {
                Console.WriteLine("Error caught, invalid message format.");
                return;
            }

            if (message == null)
                return;

            string msg = message["message"]["contents"].ToString();

            // Message does not contain a link to youtube in it
            if (!URL_MATCH.IsMatch(msg)) {
                return;
            }

            JObject snip = ParseYoutubeData(message);
            if (snip == null)
                return;

            JObject videoDetails = snip["snippet"] as JObject;
            JObject stats = snip["statistics"] as JObject;

            DateTime published = DateTime.Parse(videoDetails["publishedAt"].ToString());

            Message response = new Message(message["location"].ToObject<Guid>(), new User(this.Name), "");
            response.message = "\"" + videoDetails["title"] + "\" published by " + videoDetails["channelTitle"] + " on " + published.ToString("MMMM dd, yyyy") + ". (Views: " + String.Format("{0:N0}", int.Parse(stats["viewCount"].ToString())) + ")";

            Console.WriteLine("Recieved video information for " + videoDetails["title"]);
            SendMessage(response);
        }

        private JObject ParseYoutubeData(JObject msg) {
            Match url = URL_MATCH.Match(msg["message"]["contents"].ToString());

            string videoID = url.Groups["id"].Value.ToString().Trim();

            try {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.googleapis.com/youtube/v3/videos?part=snippet%2Cstatistics&id=" + videoID + "&key=AIzaSyCMqP8c9ww9hF4NKazgOevHtOc9uy17V5I");
                request.Credentials = CredentialCache.DefaultCredentials;
                request.UserAgent = "Suibhne Youtube Plugin";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream resStream = response.GetResponseStream();

                StreamReader sr = new StreamReader(resStream);
                string data = sr.ReadToEnd();

                JObject json = JObject.Parse(data);

                if (int.Parse(json["pageInfo"]["totalResults"].ToString()) == 0)
                    return null;

                JObject snip = json["items"][0] as JObject;
                return snip;
            }

            catch (Exception) {
                return null;
            }
            
        }
    }
}
